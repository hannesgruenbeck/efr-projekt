# Automate gmail Account creator

# import modules
import pyautogui as py
import time, random, string, webbrowser, names

def full_name():
    ## Create a random full name
    global firstName
    global lastName
    ## create a random first fame
    firstName = names.get_first_name()
    ## create a random last name
    lastName = names.get_last_name()

def email():
    global emailAdress
    ## create a email adress with our names
    emailAdress = '243' + firstName.lower() + ('.') +  lastName.lower() + '432'

def create_pw():
    # create a random password
    global password
    source = string.ascii_letters + string.digits
    password = ''.join((random.choice(source) for i in range(10)))

def telefone_number():
    global telefonnummer
    # type a telefone number
    # Fehler, falls nicht nur Zahlen getippt wurden
    try:
        telefonnummer = int(input("Bitte geben Sie Ihre Telefonnummer für die gmail Verifikation an. \n Im Format 157 123456789: "))#
    except:
        print('\n\n\n\n\nSie haben nicht nur Zahlen angegeben, bitte erneut das Programm starten!!\n\n'.upper())
    telefonnummer = str(telefonnummer)
    print('Ihre angegebene Telefonnummer lautet: ' + telefonnummer)

def create_gmail_account(fName, lName, email, pw, telefonenumber):
    # Vornamen schreiben
    time.sleep(3)
    py.write(fName)
    time.sleep(0.3)
    # Nachnamen schreiben
    py.write('\t')
    py.write(lName)
    time.sleep(0.3)
    # Email schreiben
    py.write('\t')
    py.write(email)
    py.write('\t')
    py.write('\t')
    time.sleep(0.3)
    # Passwort schreiben
    py.write(pw)
    py.write('\t')
    time.sleep(0.3)
    # Passwort bestätigen
    py.write(pw)
    py.write('\t')
    py.write('\t')
    time.sleep(0.3)

    # Seite bestätigen
    py.write('\n')
    py.click(749,556)
    time.sleep(3)
    # Telefonnummer angeben
    py.click()
    py.write(telefonenumber)

def show_data(fName, lName, email, pw, telefonenumber):
    print(f'Die Daten Ihren neuen gmail Kontos lauten: \n\nVorname: {fName} \n\nNachname: {lName} \n\nEmail Adresse: {email} \n\nPasswort {pw} \n\nTelefonnummer: {telefonenumber}')

# Funktionen um Variablen zu erzeugen
full_name()
email()
create_pw()
telefone_number()
show_data(firstName, lastName, emailAdress, password, telefonnummer)

time.sleep(3)
# Webbrowser öffnen
url = 'https://accounts.google.com/signup/v2/webcreateaccount?flowName=GlifWebSignIn&flowEntry=SignUp'
webbrowser.open(url, new = 1, autoraise = True)
# Variablen in das Formular schreiben
create_gmail_account(firstName, lastName, emailAdress, password, telefonnummer)